package com.epam.controller;

import com.epam.helper.Helper;
import com.epam.model.Minesweeper;

public class Controller {
    private Minesweeper minesweeper;

    public Controller() {
    }

    public void fillBoard(int rows, int columns, double probability) {
        minesweeper = new Minesweeper(rows, columns);
        boolean[][] arr = new boolean[rows][columns];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = Math.random() < probability;
            }
        }
        minesweeper.setBoard(arr);
    }

    public char[][] getCharBoard() {
        char[][] charBoard = new char[minesweeper.getRows()][minesweeper.getColumns()];
        for (int i = 0; i < minesweeper.getBoard().length; i++) {
            for (int j = 0; j < minesweeper.getBoard()[i].length; j++) {
                if (minesweeper.getBoard()[i][j]) {
                    charBoard[i][j] = Helper.ASTERISK;
                } else {
                    charBoard[i][j] = Helper.WHITESPACE;
                }
            }
        }
        return charBoard;
    }

    public char[][] getNeighbourMinesNumber() {
        char[][] charBoard = new char[minesweeper.getRows()][minesweeper.getColumns()];
        for (int i = 0; i < minesweeper.getBoard().length; i++) {
            for (int j = 0; j < minesweeper.getBoard()[i].length; j++) {
                if (!minesweeper.getBoard()[i][j]) {
                    charBoard[i][j] = minesAroundNumber(i, j);
                } else {
                    charBoard[i][j] = Helper.WHITESPACE;
                }
            }
        }
        return charBoard;
    }

    private char minesAroundNumber(int x, int y) {
        int numberOfMinesAround = 0;
        numberOfMinesAround += minesweeper.checkMineAtPosition(x - 1, y - 1);
        numberOfMinesAround += minesweeper.checkMineAtPosition(x - 1, y);
        numberOfMinesAround += minesweeper.checkMineAtPosition(x - 1, y + 1);
        numberOfMinesAround += minesweeper.checkMineAtPosition(x, y + 1);
        numberOfMinesAround += minesweeper.checkMineAtPosition(x + 1, y + 1);
        numberOfMinesAround += minesweeper.checkMineAtPosition(x + 1, y);
        numberOfMinesAround += minesweeper.checkMineAtPosition(x + 1, y - 1);
        numberOfMinesAround += minesweeper.checkMineAtPosition(x, y - 1);
        return Character.forDigit(numberOfMinesAround, Helper.DECIMAL);
    }
}
