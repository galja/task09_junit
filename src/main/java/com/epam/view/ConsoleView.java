package com.epam.view;

import com.epam.controller.ConsoleScanner;
import com.epam.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import com.epam.helper.Helper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements View {
    private static Logger logger = LogManager.getLogger(ConsoleView.class);
    private Controller controller;
    private ConsoleScanner scanner;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public ConsoleView() {
        controller = new Controller();
        scanner = new ConsoleScanner();
        generateMenu();
        putMethods();
    }

    private  void initializeBoard(){
        logger.info("Enter number of rows:\n");
        int rows = scanner.scanAnInteger();
        logger.info("Enter number of columns:\n");
        int columns = scanner.scanAnInteger();
        logger.info("Enter percentage of mines:\n");
        int probability = scanner.scanADouble();
        controller.fillBoard(rows, columns, probability);
    }
    private void outputMenu() {
        logger.info("\nMENU\n");
        for (String str : menu.values()) {
            logger.info("\n" + str);
        }
        logger.info("Enter Q to exit");
    }

    public void show() {
        Scanner in = new Scanner(System.in);
        String keyMenu;
        initializeBoard();
        do {
            outputMenu();
            logger.info("\nPlease, select menu point:\n");
            keyMenu = in.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Print board with * where mines are");
        menu.put("2", "2 - Print board with with number of mines around");
    }

    private void putMethods() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printCharBoard);
        methodsMenu.put("2", this::printNeighbourMinesNumber);
    }

    private void printNeighbourMinesNumber() {
        logger.info(arrayToString(controller.getNeighbourMinesNumber()));
    }

    private void printCharBoard() {
        logger.info(arrayToString(controller.getCharBoard()));
    }


    private String arrayToString(char[][] array){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                stringBuilder.append(controller.getCharBoard()[i][j]).append(Helper.TAB);
            }
            stringBuilder.append(Helper.NEW_LINE);
        }
        return stringBuilder.toString();
    }
}
