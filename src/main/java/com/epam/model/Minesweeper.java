package com.epam.model;

public class Minesweeper {
    private int rows;
    private int columns;
    private boolean[][] board;
    //private double probability;

    public Minesweeper(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.board = new boolean[rows][columns];
        //this.probability = probability;
        //fillArray();
    }

    public void setBoard(boolean[][] board) {
        this.board = board;
    }

    public boolean[][] getBoard() {
        return this.board;
    }

//    private void fillArray() {
//        for (int i = 0; i < board.length; i++) {
//            for (int j = 0; j < board[i].length; j++) {
//                board[i][j] = getMine();
//            }
//        }
//    }

    public int getRows() {
        return this.rows;
    }

    public int getColumns() {
        return this.columns;
    }

    public int checkMineAtPosition(int x, int y) {
        if ((x >= 0) && (y >= 0) && (y < columns) && (x < rows) && (this.board[x][y])) {
            return 1;
        } else {
            return 0;
        }
    }

//    private boolean getMine() {
//        return Math.random() < this.probability;
//    }
}
