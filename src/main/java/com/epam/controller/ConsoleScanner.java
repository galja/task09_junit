package com.epam.controller;

import com.epam.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleScanner {

    private static Logger logger = LogManager.getLogger(View.class);

    private Scanner scanner;
    private int intValue;
    private double doubleValue;

    public ConsoleScanner() {
        scanner = new Scanner(System.in);
        intValue = 0;
        doubleValue = 0;
    }

    public int scanAnInteger() {
        try {
            if((intValue = scanner.nextInt())<0){
                throw new Exception("Input can't be negative");
            }
        } catch (InputMismatchException e) {
            logger.warn("Input must be an integer. Try again");
            scanAnInteger();
        } catch (Exception e) {
            logger.warn(e);
        }
        return intValue;
    }

    public int scanADouble() {
        try {
            if((doubleValue = scanner.nextDouble())<0){
                throw new Exception();
            }
        } catch (InputMismatchException e) {
            logger.warn("Input must be a double. Try again\n");
            scanADouble();
        } catch (Exception e) {
            logger.warn("Input can't be negative\n");
        }
        return intValue;
    }
}
