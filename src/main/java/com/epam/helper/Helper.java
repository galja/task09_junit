package com.epam.helper;

public class Helper {
    public static final char WHITESPACE = ' ';
    public static final char ASTERISK = '*';
    public static final int DECIMAL = 10;
    public static final String TAB = "\t";
    public static final String NEW_LINE = "\n";
}
